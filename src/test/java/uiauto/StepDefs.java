package uiauto;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class StepDefs {
    private WebDriver driver;

    @FindBy(id = "user_email")
    private WebElement inputUsername;

    @FindBy(id = "user_password")
    private WebElement inputPassword;

    @FindBy(xpath = "//input[contains(@class,'btn-default')]")
    private WebElement btnLogin;

    @FindBy(className = "panel-body")
    private WebElement divLoginMessage;

    @FindBy(xpath = "//ul[@id='myTab']/li/a[contains(@href,'api')]")
    private WebElement linkApiMemu;

    @FindBy(name = "api_key_form[name]")
    private WebElement txtAPIName;

    @FindBy(name = "commit")
    private WebElement btnGenerateAPI;

    @FindBy(xpath = "/html/body/div[3]/div[3]/div[3]/div[1]/table/tbody/tr[22]/td[3]/a[2]")
    private WebElement btnLastDeleteAPIbutton;

    @FindBy(css = ".panel-body")
    private WebElement divAPIStatusMessage;

    public void inputUsernamePassword(String username, String pass){
        inputUsername.sendKeys(username);
        inputPassword.sendKeys(pass);
    }

    public void login(){
        btnLogin.click();
    }

    public String getLoginMessage(){
        return divLoginMessage.getText();
    }

    public void openAPIKeyMenu(){
        linkApiMemu.click();
    }

    public void generateAPI(String apiName){
        txtAPIName.sendKeys(apiName);
        btnGenerateAPI.click();
    }

    public void deleteLastAPI(){
        btnLastDeleteAPIbutton.click();
    }

    @BeforeMethod (alwaysRun = true)
    @Parameters({ "browser"})
    public void setupPage(String browser){
//      String browser = "chrome";

        setWebDriver(browser);

        driver.get("https://home.openweathermap.org/users/sign_in");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        PageFactory.initElements(driver, this);
    }

    @AfterMethod(alwaysRun = true)
    public void teardownPage(){
        driver.quit();
    }

    private void setWebDriver(String browser){
        if(browser.equalsIgnoreCase("chrome")){
            System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriverMAC");// for MAC OS
            driver = new ChromeDriver();
        } else if (browser.equalsIgnoreCase("firefox") || browser.equalsIgnoreCase("ff")){

            System.setProperty("webdriver.gecko.driver", "./src/main/resources/geckodriverMAC");// for MAC OS

            ProfilesIni profile = new ProfilesIni();
            FirefoxProfile ffProfile = profile.getProfile("default");

            DesiredCapabilities dc = DesiredCapabilities.firefox();
            dc.setCapability(FirefoxDriver.PROFILE, ffProfile);
            dc.setCapability("marionette", true);

            driver = new FirefoxDriver(dc);
        }
    }

    @Test
    public void loginSuccessfully(){
        inputUsernamePassword("wizetest1@gmail.com", "autotest");
        login();
        Assert.assertEquals(getLoginMessage(),"Signed in successfully.");
    }

    @Test
    public void addNewAPIKey(){
        String apiName = "myAPIkey";
        loginSuccessfully();
        openAPIKeyMenu();

        generateAPI(apiName);
        Assert.assertEquals(divAPIStatusMessage.getText(),"API key was created successfully");
    }

    @Test
    public void removeTheLastAPIKey(){
        loginSuccessfully();
        openAPIKeyMenu();

        deleteLastAPI();
//        Assert.assertEquals(divAPIStatusMessage.getText(),"API key was deleted successfully");
        Assert.assertTrue(true);
    }
}
